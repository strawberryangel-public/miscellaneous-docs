# Sophie’s Wardrobe
*Or, How to Dress Weird in SL*

I’ve been asked from time to time where to buy gothic, Victorian or steampunk clothes. 
This is a snapshot as of 9/23/2016.

## How to Dress

As far as style, I can’t really give advice here. Being goth is a mindset and aesthetic sense. If you’re goth  you already have the aesthetic tastes. If you don’t, I can’t teach you to be or look goth without embarrassing us both. Just because you’re wearing black may simply mean you look like a dorky emo wannabe from South Park.

## How to Shop

Demos. Demos. Demos. Beyond that, become a queen of mix and match. Keep an eye out for a skirt, a bracelet, a bolero, etc. that catches your eye and you can imagine building an outfit around that, or can put with things you have. Creativity is a part of the mindset. But you already knew that.

## Where to Shop

I have clothes for old style (“classic”) bodies and mesh.

### Both Mesh and Classic

The
[Laced & Strapped Black Leather Heels](https://marketplace.secondlife.com/p/Laced-Strapped-Black-Leather-Heels/2108175)
are free, look terrific and work on both bodies.

### Classic Body

<strike>The good news about the classic body is that there is a lot of free and cheap goth clothes. Lapoint & Bastchild (L&B) has an entire store of free classic clothes in the southwest corner of their sim (to the left of Death Royale).</strike> This little store is gone.

<strike>If you’re willing to drop some L$, then AVid and RFyre are by far my favorite places for classic body.</strike> These sims are gone.

Also, many places on the mesh list below have classic body clothes, or accessories, etc. that work just great.

### Mesh

I have a Maitreya Laura body.
This is a sampling of the places I’ve gotten my Maitreya wardrobe from.
**This isn’t a complete list.**
It’s pretty random.
Nor does it include my furry and anime demon girl outfits based on the Kemono body.
Nor my classic body outfits.

However, this should be enough to get you started.
These are listed alphabetically, with some notes.
If you know of more, let me know.
I also haven’t listed BDSM shops.
They’re two different worlds.
However, if you’re into that and you find something that  tickles your gothy aesthetic, run with it.
(I certainly do.)


| Location | What | Notes |
|:-- |:-- |:-- |
asiling | Accessories | chestlaces, body jewelry, headdresses
[AviCandy](http://maps.secondlife.com/secondlife/Meds/178/77/24) | Hose, Stockings, Nail | Excelent place.
[Black Arts](http://maps.secondlife.com/secondlife/Galien/150/99/29) | Clothes/Accessories | Goth & Vamp & Retro
Blueberry | Clothes | Not a goth place but some solid finds here.
CATWA | Hair | 
Chic & Shoes | Boots | 
Cog & Fleur | Clothes | Victorian & Steampunk
Crazy Kitty | Clothes | Goth & Punk & Bad Girl
Cute or Die | Clothes | 
Damoour |  | 
[DE Designs](http://maps.secondlife.com/secondlife/DE%20Designs/115/134/39) | Clothes | Excellent place.
Dead Dollz | Clothes | Great stuff. Some goth & alternative.
elikatira | Hair | 
Entice | Clothes | Also, swimsuits
erratic | Clothes, Shoes, Accessories | Excellent mesh, swimsuits and big girl clothes
Etchaflesh Clothing & Corsetry | Clothing, Shoes, Accessories | Solid place.
FashionNatic | Clothes | 
Gaall |  | 
[IKON](http://maps.secondlife.com/secondlife/IKON/143/128/501) | Eyes | 
insanya | Clothes | 
Just Because |  | Not a goth place. A couple of pieces for mix and match.
KC Couture | Shoes | Excellent shoes and boots.
Kitty Creations |  | I have thigh boots from here.
Kreepshow Kouture | Clothes | 
Lindy Modern & Retro Shoes | Boots | A favorite pair of biker calf-high boots. (Thora)
Mette's Design | Clothes | I have a nice trench coat.
MOZ | Clothes | Not a goth store, but a couple of finds.
MUKA | Accessories | Harnesses, thongs, straps, cuffs, collars, other big girl items.
Pandora Design | Clothes | I have one long skirt from here.
Purr & Chase | Clothes | I have pre-release Maitreya versions. The Kemono clothes are great.
RealEvil Industries | Jewelry/Gloves | Beautiful workmanship
REIGN | Shoes | 
Renegade Shoes | Boots | 
Rezology | Hair | 
Synyster Creations | Clothes | Goth & Punk & Vamp
Tiar |  | I have a nice leather bolero from here.
Vaxer |  | 



